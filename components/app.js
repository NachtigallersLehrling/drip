import React, { useState, useEffect } from 'react'
import { BackHandler, StyleSheet, View, DeviceEventEmitter } from 'react-native'
import PropTypes from 'prop-types'

import { LocalDate } from '@js-joda/core'

import Header from './header'
import Menu from './menu'
import { viewsList } from './views'
import { pages } from './pages'

import setupNotifications from '../lib/notifications'
import { closeDb, checkIfDateHasTemperature, saveSymptom } from '../db'

const App = ({ restartApp }) => {
  const [date, setDate] = useState(LocalDate.now().toString())
  const [currentPage, setCurrentPage] = useState('Home')
  const goBack = () => {
    if (currentPage === 'Home') {
      closeDb()
      BackHandler.exitApp()
    } else {
      const { parent } = pages.find((p) => p.component === currentPage)

      setCurrentPage(parent)
    }

    return true
  }

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      goBack
    )

    return () => backHandler.remove()
  })

  useEffect(() => setupNotifications(setCurrentPage, setDate), [])

  const Page = viewsList[currentPage]
  const isTemperatureEditView = currentPage === 'TemperatureEditView'
  const headerProps = { navigate: setCurrentPage }
  const pageProps = {
    date,
    setDate,
    isTemperatureEditView,
    navigate: setCurrentPage,
  }

  useEffect(() => {
    DeviceEventEmitter.addListener('temperature-data-received', (bundle) => {
      const data = JSON.parse(bundle['android.intent.extra.TEXT'])
      //console.log('Received Measurement: ', data)
      // checks if the day has already a recorded temperature
      const measurement_date = data['Date']
      if (!checkIfDateHasTemperature(measurement_date)) {
        // save reported temperature
        const valuesToSave = {
          exclude: false,
          note: '',
          time: data['Time'],
          value: Number(data['Temperature']),
        }
        //console.log(' Saving Temperature: ', valuesToSave, ' on date ', measurement_date)
        saveSymptom('temperature', measurement_date, valuesToSave)
      } else {
        //console.log(' Ignoring Measurement: ', data)
      }
    })
  }, [])

  return (
    <View style={styles.container}>
      <Header {...headerProps} />
      <Page {...pageProps} restartApp={restartApp} />
      <Menu currentPage={currentPage} navigate={setCurrentPage} />
    </View>
  )
}

App.propTypes = {
  restartApp: PropTypes.func,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default App
